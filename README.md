# The on-demand warehousing optimization problem #

This repository contains the instances of the problem presented in the paper
*On-demand warehousing optimization problem* by Sara Ceschia,
Margaretha Gansterer, Simona Mancini and Antonella Meneghetti,
published in *International Journal of Production Research*, DOI:
[10.1080/00207543.2022.2078249](https://doi.org/10.1080/00207543.2022.2078249),
2022.

### Dataset ###

The reference scenario and the artificial instances are stored in folder
[`Dataset`](Dataset) in txt format.


